package com.example.recipeapp.data

import com.example.recipeapp.application.Recipe.model.RecipeInfoArray
import com.example.recipeapp.application.Recipe.model.RecipeInfo
import com.example.recipeapp.application.authentication.model.User
import com.google.gson.annotations.SerializedName


class Root {
    @SerializedName("status")
    private var status: Boolean? = null
    @SerializedName("message")
    private var message: String? = null
    @SerializedName("userInfo")
    private var userInfo: User? = null
    @SerializedName("token")
    private var token: String? = null
    @SerializedName("recipeInfo")
    private var recipeInfo: RecipeInfo? = null
    @SerializedName("allRecipeInfo")
    private var recipeInfoArray: List<RecipeInfoArray>? = null
    fun getRecipeInfoArray(): List<RecipeInfoArray>? {
        return recipeInfoArray
    }
    fun setRecipeInfoArray(recipeInfo: List<RecipeInfoArray>) {
        this.recipeInfoArray = recipeInfo
    }

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getUserInfo(): User? {
        return userInfo
    }

    fun setUserInfo(userInfo: User) {
        this.userInfo = userInfo
    }

    fun getRecipeInfo(): RecipeInfo? {
        return recipeInfo
    }

    fun setUserInfo(recipeInfo: RecipeInfo) {
        this.recipeInfo = recipeInfo
    }

    fun getToken(): String? {
      return token;
    }

    fun setToken(token: String)
    {
        this.token=token
    }
}

package com.example.recipeapp.database

import android.content.SharedPreferences


class LocalStorage {

    private val sInstance: LocalStorage = LocalStorage()
    private var mSharedPreferences: SharedPreferences? = null
    private val PREF_LOGGED_IN = "logged_in"

    public val SHARED = "shared"
}
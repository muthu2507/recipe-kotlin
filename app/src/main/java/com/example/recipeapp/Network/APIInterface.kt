package com.example.recipeapp.network

import com.example.recipeapp.data.Root
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable

interface APIInterface {

    @POST("users/login")
    fun signInByEmail(@Body requestBody: RequestBody?): Observable<Root?>?

    @POST("users/register")
    fun signUp(@Body requestBody: RequestBody?): Observable<Root?>?

    @GET("recipe/findOne/{recipeID}")
    fun singleRecipe(@Path("recipeID") recipeID: String?): Observable<Root?>?

    @GET("recipe/find/{recipeType}")
    fun recipeDetails(@Path("recipeType") recipeType: String?): Observable<Root?>?

    @DELETE("recipe/delete/{recipeID}")
    fun deleteRecipe(@Path("recipeID") recipeID: String?): Observable<Root?>?

    @POST("recipe/create")
    @Multipart
    fun createRecipe(
        @Part("dish") dish: RequestBody?,
        @Part("ingredients") ingredients: RequestBody?,
        @Part("steps") steps: RequestBody?,
        @Part("recipe_type") type: RequestBody?,
        @Part file: MultipartBody.Part?
    ): Observable<Root?>?
}
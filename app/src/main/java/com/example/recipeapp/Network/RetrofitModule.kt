package com.example.recipeapp.Network

import com.example.recipeapp.network.APIInterface
import com.example.recipeapp.network.ApplicationScope
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.android.BuildConfig


@Module
class RetrofitModule {

    @Provides
    @ApplicationScope
    fun getInterface(retrofit:Retrofit): APIInterface {
        return retrofit.create(APIInterface::class.java)
    }

    @Provides
    @ApplicationScope
    fun getRetrofit(okHttpClient:OkHttpClient):Retrofit {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://ec2-52-76-35-182.ap-southeast-1.compute.amazonaws.com:8081/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(okHttpClient)
            .build()
        return retrofit
    }

    fun getRetrofitWithBaseUrl(okHttpClient:OkHttpClient, baseUrl:String):Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @ApplicationScope
    fun getOkHttpClient(httpLoggingInterceptor:HttpLoggingInterceptor):OkHttpClient {
        val interceptor = object: Interceptor {
            override fun intercept(chain: Interceptor.Chain):okhttp3.Response {
                val newRequest = chain.request().newBuilder()
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build()
                return chain.proceed(newRequest)
            }
        }

        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    @Provides
    @ApplicationScope
    fun httpLoggingInterceptor():HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
        return httpLoggingInterceptor
    }



    fun getApiInterface(): APIInterface {
            val retrofitModule = RetrofitModule()
            val okHttpClient = retrofitModule.getOkHttpClient(retrofitModule.httpLoggingInterceptor())
            val retrofit = retrofitModule.getRetrofit(okHttpClient)
            return retrofitModule.getInterface(retrofit)
    }

    fun getApiInterface(baseUrl:String): APIInterface {
        val retrofitModule = RetrofitModule()
        val okHttpClient = retrofitModule.getOkHttpClient(retrofitModule.httpLoggingInterceptor())
        val retrofit = retrofitModule.getRetrofitWithBaseUrl(okHttpClient, baseUrl)
        return retrofitModule.getInterface(retrofit)
    }
}
package com.example.recipeapp.application.authentication.model



import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("_id")

    private var id: String? = null
    @SerializedName("username")

    private var username: String? = null
    @SerializedName("password")

    private var password: String? = null
    @SerializedName("email")

    private var email: String? = null
    @SerializedName("createdAt")

    private var createdAt: String? = null
    @SerializedName("updatedAt")

    private var updatedAt: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String?) {
        this.username = username
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String?) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String?) {
        this.updatedAt = updatedAt
    }

}
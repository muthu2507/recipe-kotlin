package com.example.recipeapp.application.authentication.presenter

interface AuthActivityContract {
    interface Presenter {
        fun onFetchLoginDetails(email: String, password: String)
    }
    interface RegisterPresenter {
        fun onFetchRegisterDetails(username: String, password: String, email: String)
    }

    interface View {
        fun onNext(msg: String, status: Boolean)
        fun showError(msg: String)
    }

}
package com.example.recipeapp.application.authentication.presenter

import android.content.Context
import android.util.Log
import com.example.recipeapp.application.authentication.view.LoginActivity
import com.example.recipeapp.data.Root
import com.example.recipeapp.network.APIInterface
import com.example.recipeapp.Network.RetrofitModule
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class AuthPresenterImpl : AuthActivityContract.Presenter {

    private var loginActivity: LoginActivity
    private var mAPIInterface: APIInterface
    private var retrofitModule = RetrofitModule()

    constructor(view: LoginActivity) {
        this.loginActivity = view
        mAPIInterface = retrofitModule.getApiInterface()
    }


    override fun onFetchLoginDetails(email: String, password: String) {
        val `object` = JSONObject()
        try {
            `object`.put("password", password)
            `object`.put("emailaddress", email)
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
        Log.d("Tag1", "entering layer 3")

        val body = RequestBody.create(MediaType.parse("raw"), `object`.toString())
        Log.d("Tag1", body.toString())
        mAPIInterface.signInByEmail(body)!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<Root>() {

                override fun onCompleted() {

                }
                override fun onError(e: Throwable) {
                    loginActivity.showError(e.toString())
                   
                }
                override fun onNext(t: Root) {

                    val sharedPref = loginActivity?.getSharedPreferences(
                       "SHARED", Context.MODE_PRIVATE)

                    var editor = sharedPref.edit()
                    editor.putBoolean("loggedin",true)
                    editor.commit()

                   loginActivity.onNext(t.getMessage().toString(), t.getStatus()!!)
                }
            })



    }
}
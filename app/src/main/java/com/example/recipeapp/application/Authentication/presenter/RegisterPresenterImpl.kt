package com.example.recipeapp.application.authentication.presenter

import android.util.Log
import com.example.recipeapp.application.authentication.view.RegisterActivity
import com.example.recipeapp.data.Root
import com.example.recipeapp.network.APIInterface
import com.example.recipeapp.Network.RetrofitModule
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class RegisterPresenterImpl : AuthActivityContract.RegisterPresenter {

    private var registerActivity: RegisterActivity
    private var mAPIInterface: APIInterface
    private var retrofitModule = RetrofitModule()

    constructor(view: RegisterActivity) {
        this.registerActivity = view
        mAPIInterface = retrofitModule.getApiInterface()
    }



    override fun onFetchRegisterDetails(username: String, password: String, email: String) {
        val `object` = JSONObject()
        try {
            `object`.put("password", password)
            `object`.put("emailaddress", email)
            `object`.put("username", username)

        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
        Log.d("Tag1", "entering layer 3")

        val body = RequestBody.create(MediaType.parse("raw"), `object`.toString())
        Log.d("Tag1", body.toString())
        mAPIInterface.signUp(body)!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<Root>() {

                override fun onCompleted() {

                }
                override fun onError(e: Throwable) {
                    registerActivity.showError(e.toString())

                }
                override fun onNext(t: Root) {
                    registerActivity.onNext(t.getMessage().toString(), t.getStatus()!!)
                }
            })

    }
}
package com.example.recipeapp.application.authentication.view

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.recipeapp.R
import com.example.recipeapp.application.Recipe.view.MainMenuActivity
import com.example.recipeapp.application.authentication.presenter.AuthActivityContract
import com.example.recipeapp.application.authentication.presenter.AuthPresenterImpl


class LoginActivity : AppCompatActivity(), AuthActivityContract.View {

    private var mAuthPresenter: AuthPresenterImpl? = null
    private lateinit var btnLogin: Button
    private lateinit var txtSignUp: TextView
    private lateinit var txtEmail: EditText
    private lateinit var txtPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin = findViewById(R.id.btnLogin)
        txtSignUp = findViewById(R.id.tvCrtaccount)
        txtEmail = findViewById(R.id.etEmail)
        txtPassword = findViewById(R.id.etPassword)

        mAuthPresenter = AuthPresenterImpl(this)
        btnLogin.setOnClickListener {

            Log.d("TAG", "entering 2")
            if (cannotBeEmpty()) {
                if (validateEmail(txtEmail.text)) saveLoginDetails() else Toast.makeText(
                    applicationContext,
                    "Invalid Email Format",
                    Toast.LENGTH_SHORT
                ).show()
            } else Toast.makeText(
                applicationContext,
                "Email Address or Password cannot be empty",
                Toast.LENGTH_SHORT
            ).show()
        }
        txtSignUp.setOnClickListener {
            startActivity(
                Intent(
                    this@LoginActivity,
                    RegisterActivity::class.java
                )
            )
        }
    }

    override fun onNext(msg: String, status: Boolean) {

        if (!status) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()


        } else {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
            startActivity(
                Intent(
                    this,
                    MainMenuActivity::class.java
                )
            )
        }

    }

    override fun showError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    private fun validateEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    private fun cannotBeEmpty(): Boolean {
        return !TextUtils.isEmpty(txtEmail.text) && !TextUtils.isEmpty(txtPassword.text)
    }

    private fun saveLoginDetails() {
        Log.d("Tag1", "entering layer 1")
        Log.d("Tag2", txtEmail.text.toString())
        mAuthPresenter?.onFetchLoginDetails(
            txtEmail.text.toString(),
            txtPassword.text.toString()
        )
    }


}
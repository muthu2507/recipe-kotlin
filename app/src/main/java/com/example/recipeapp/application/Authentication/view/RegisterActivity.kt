package com.example.recipeapp.application.authentication.view

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.recipeapp.R
import com.example.recipeapp.application.Recipe.view.MainMenuActivity
import com.example.recipeapp.application.authentication.presenter.AuthActivityContract
import com.example.recipeapp.application.authentication.presenter.AuthPresenterImpl
import com.example.recipeapp.application.authentication.presenter.RegisterPresenterImpl

class RegisterActivity : AuthActivityContract.View, AppCompatActivity() {


    private var mAuthPresenter: RegisterPresenterImpl? = null
    private lateinit var tvCancel : TextView
    private lateinit var txtSignUp : Button
    private  lateinit var txtEmail : EditText
    private  lateinit var txtUsername : EditText
    private lateinit var txtPassword : EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        tvCancel  = findViewById(R.id.tvCancel)
        txtSignUp = findViewById(R.id.btnCrtAccount)
        txtEmail = findViewById(R.id.etEmail)
        txtPassword = findViewById(R.id.etPassword)
        txtUsername = findViewById(R.id.etUsername)

        mAuthPresenter = RegisterPresenterImpl(this)
        txtSignUp.setOnClickListener {

            Log.d("TAG", "entering 2")
            if (cannotBeEmpty()) {
                if (validateEmail(txtEmail.text)) saveDetails() else Toast.makeText(
                    applicationContext,
                    "Invalid Email Format",
                    Toast.LENGTH_SHORT
                ).show()
            } else Toast.makeText(
                applicationContext,
                "Email Address, Username or Password cannot be empty",
                Toast.LENGTH_SHORT
            ).show()
        }
        tvCancel.setOnClickListener {
         finish()
        }



    }


    override fun onNext(msg: String, status: Boolean) {

        if (!status)
        {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }
        else
        {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    override fun showError(msg: String) {

    }

    private fun validateEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    private fun cannotBeEmpty(): Boolean {
        return !TextUtils.isEmpty(txtEmail.text) && !TextUtils.isEmpty(txtPassword.text) && !TextUtils.isEmpty(txtUsername.text)
    }

    private fun saveDetails() {
        Log.d("Tag1", "entering layer 1")
        Log.d("Tag2", txtEmail.text.toString())
        mAuthPresenter?.onFetchRegisterDetails(
            txtUsername.text.toString(),
            txtPassword.text.toString(),
            txtEmail.text.toString()
        )
    }
}
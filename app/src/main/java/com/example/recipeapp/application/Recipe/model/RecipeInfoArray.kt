package com.example.recipeapp.application.Recipe.model

import com.google.gson.annotations.SerializedName

class RecipeInfoArray {
    @SerializedName("_id")
    private var id: String? = null
    @SerializedName("dish")
    private var dish: String? = null
    @SerializedName("ingredient")
    private var ingredient: String? = null
    @SerializedName("steps")
    private var steps: String? = null
    @SerializedName("recipe_type")
    private var recipeType: String? = null
    private var image: String? = null
    @SerializedName("__v")
    private var v: Int? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getDish(): String? {
        return dish
    }

    fun setDish(dish: String?) {
        this.dish = dish
    }

    fun getIngredient(): String? {
        return ingredient
    }

    fun setIngredient(ingredient: String?) {
        this.ingredient = ingredient
    }

    fun getSteps(): String? {
        return steps
    }

    fun setSteps(steps: String?) {
        this.steps = steps
    }

    fun getRecipeType(): String? {
        return recipeType
    }

    fun setRecipeType(recipeType: String?) {
        this.recipeType = recipeType
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String?) {
        this.image = image
    }

    fun getV(): Int? {
        return v
    }

    fun setV(v: Int?) {
        this.v = v
    }

}
package com.example.recipeapp.application.Recipe.model


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recipeapp.R


class RecipeListAdapter() : RecyclerView.Adapter<RecipeListAdapter.ViewHolder>() {
    private lateinit var mRecipeInfo: List<RecipeInfoArray>
    private lateinit var mCallback : RecipeItemClickListener
    constructor(context: Context?, recipeInfo: List<RecipeInfoArray>?) : this()
    {
        if (recipeInfo != null) {
            this.mRecipeInfo = recipeInfo
        }
    }


    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        var txtRecipeList: TextView = mView.findViewById(R.id.tvRecipeList)
        var llRecipeItem: LinearLayout = mView.findViewById(R.id.llRecipeItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.recipe_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val recipeInfo: RecipeInfoArray = mRecipeInfo[position]
        holder.txtRecipeList.text=recipeInfo.getDish()
        holder.llRecipeItem.setOnClickListener{

            mCallback.onItemClick(recipeInfo.getId()!!)
        }
    }

    override fun getItemCount(): Int {
      return mRecipeInfo.size
    }

    fun setOnItemClickListener(listener: RecipeItemClickListener) {
        mCallback = listener
    }

    interface RecipeItemClickListener {
        fun onItemClick(recipeID: String)
    }


}

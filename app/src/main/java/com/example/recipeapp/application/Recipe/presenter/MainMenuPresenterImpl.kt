package com.example.recipeapp.application.Recipe.presenter

import android.util.Log
import com.example.recipeapp.application.Recipe.view.MainMenuActivity
import com.example.recipeapp.data.Root
import com.example.recipeapp.network.APIInterface
import com.example.recipeapp.Network.RetrofitModule
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class MainMenuPresenterImpl {

    private var mainMenuActivity: MainMenuActivity
    private var mAPIInterface: APIInterface
    private var retrofitModule = RetrofitModule()

    constructor(view: MainMenuActivity) {
        this.mainMenuActivity = view
        mAPIInterface = retrofitModule.getApiInterface()
    }

    fun onDisplayRecipeDetails(recipeType:String)
    {
        mAPIInterface.recipeDetails(recipeType)!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<Root>() {

                override fun onCompleted() {

                }
                override fun onError(e: Throwable) {
                    Log.d("TAG1", e.toString())
                    mainMenuActivity.showError(e.toString())

                }
                override fun onNext(t: Root) {
                    mainMenuActivity.onNext(t.getMessage().toString(), t.getRecipeInfoArray())
                }
            })
    }
}
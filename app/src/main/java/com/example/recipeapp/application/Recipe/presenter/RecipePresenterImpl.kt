package com.example.recipeapp.application.Recipe.presenter

import android.net.Uri
import com.example.recipeapp.application.Recipe.view.SingleRecipeActivity
import com.example.recipeapp.application.Recipe.view.add_recipe
import com.example.recipeapp.application.Utils.FileUtil
import com.example.recipeapp.data.Root
import com.example.recipeapp.network.APIInterface
import com.example.recipeapp.Network.RetrofitModule
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File


class RecipePresenterImpl {
    private lateinit var singleRecipeActivity: SingleRecipeActivity
    private lateinit var add_recipe: add_recipe
    private var mAPIInterface: APIInterface
    private var retrofitModule = RetrofitModule()

    constructor(view: SingleRecipeActivity) {
        this.singleRecipeActivity = view
        mAPIInterface = retrofitModule.getApiInterface()
    }

    constructor(view: add_recipe) {
        this.add_recipe = view
        mAPIInterface = retrofitModule.getApiInterface()
    }


    fun onDisplaySingleRecipeDetails(id: String) {
        mAPIInterface.singleRecipe(id)!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<Root>() {

                override fun onCompleted() {

                }

                override fun onError(e: Throwable) {
                    singleRecipeActivity.showError(e.toString())

                }

                override fun onNext(t: Root) {
                    singleRecipeActivity.onNext(t.getMessage().toString(), t.getRecipeInfo()!!)
                }
            })
    }

    fun onDeleteRecipe(id: String) {
        mAPIInterface.deleteRecipe(id)!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<Root>() {

                override fun onCompleted() {

                }

                override fun onError(e: Throwable) {
                    singleRecipeActivity.showDeleteError(e.toString())

                }

                override fun onNext(t: Root) {
                    singleRecipeActivity.onDeleteNext(t.getMessage().toString(), t.getStatus()!!)
                }
            })
    }

    fun onCreateRecipe(dish: String, ingredients: String, steps: String, type: String, uri: Uri) {
        var file = File(uri.getEncodedPath())
        if (file.toString().contains("content:/")) {
            file = File(FileUtil.getInstance(add_recipe).getRealPathFromURI(add_recipe, uri))
        }

        val fbody: RequestBody = RequestBody.create(MediaType.parse("image/jpeg"), file)
        val filePart =
            MultipartBody.Part.createFormData("image", file.name, fbody)

        val `object` = JSONObject()
        try {
            `object`.put("dish", dish)
            `object`.put("ingredients", ingredients)
            `object`.put("steps", steps)
            `object`.put("type", type)
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(MediaType.parse("raw"), `object`.toString())

        mAPIInterface.createRecipe(
            RequestBody.create(okhttp3.MultipartBody.FORM, dish),
            RequestBody.create(okhttp3.MultipartBody.FORM, ingredients),
            RequestBody.create(okhttp3.MultipartBody.FORM, steps),
            RequestBody.create(okhttp3.MultipartBody.FORM, type),
            filePart
        )!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Subscriber<Root>() {

                override fun onCompleted() {

                }

                override fun onError(e: Throwable) {
                    add_recipe.showDeleteError(e.toString())

                }

                override fun onNext(t: Root) {
                    add_recipe.onDeleteNext(t.getMessage().toString(), t.getStatus()!!)
                }
            })
    }
}
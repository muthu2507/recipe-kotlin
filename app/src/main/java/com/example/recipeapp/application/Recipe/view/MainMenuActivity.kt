package com.example.recipeapp.application.Recipe.view


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recipeapp.R
import com.example.recipeapp.application.Recipe.model.RecipeInfoArray
import com.example.recipeapp.application.Recipe.model.RecipeListAdapter
import com.example.recipeapp.application.Recipe.presenter.MainMenuPresenterImpl
import com.example.recipeapp.application.Utils.SpinnerStyle


class MainMenuActivity : AppCompatActivity(), RecipeListAdapter.RecipeItemClickListener {


    lateinit var mainMenuPresenterImpl: MainMenuPresenterImpl
    private lateinit var recyclerView: RecyclerView

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        mainMenuPresenterImpl = MainMenuPresenterImpl(this)
        val spinner = findViewById<View>(R.id.spRecipeType) as Spinner
        val recipeTypes = arrayOf(
            "Choose a recipe type",
            "Dessert",
            "Main Courses",
            "Salads",
            "Snacks"
        )
        recyclerView = findViewById(R.id.rvRecipeList)

        spinner.adapter = object : SpinnerStyle(
            this,
            R.layout.spinner_style, recipeTypes
        ) {}
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedItem = parent?.getItemAtPosition(position).toString()
                if (selectedItem == "Choose a recipe type") {
                    recyclerView.adapter = null
                } else {
                    mainMenuPresenterImpl.onDisplayRecipeDetails(selectedItem)

                }
            }

        }

    }

    fun showError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }


    fun onNext(msg: String, recipeInfo: List<RecipeInfoArray>?) {
        val recipeListAdapter = RecipeListAdapter(this, recipeInfo)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = recipeListAdapter
        recipeListAdapter.setOnItemClickListener(this)
    }

    override fun onItemClick(recipeID: String) {

        val intent = Intent(this, SingleRecipeActivity::class.java)
        intent.putExtra("recipeID", recipeID)
        startActivity(intent)
        finish()

    }

    fun add_recipe(view: View) {
        startActivity(
            Intent(
                this,
                add_recipe()::class.java
            )
        )
    }


}

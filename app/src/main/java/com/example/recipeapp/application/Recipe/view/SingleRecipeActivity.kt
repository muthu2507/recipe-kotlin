package com.example.recipeapp.application.Recipe.view


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.recipeapp.R
import com.example.recipeapp.application.Recipe.model.RecipeInfo
import com.example.recipeapp.application.Recipe.presenter.RecipePresenterImpl


class SingleRecipeActivity: AppCompatActivity() {

    private var mRecipePresenterImpl: RecipePresenterImpl? = null

    private lateinit var ivBack : ImageView
    private lateinit var ivDishImage : ImageView
    private lateinit var tvRecipeName : TextView
    private lateinit var tvDishName : TextView
    private lateinit var tvIngredients : TextView
    private lateinit var tvSteps : TextView
    private lateinit var btnDelete : Button
    private lateinit var recipeID : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_recipe)
        ivBack = findViewById(R.id.ivBack)
        ivDishImage = findViewById(R.id.ivDishImage)
        tvRecipeName = findViewById(R.id.tvRecipeName)
        tvDishName = findViewById(R.id.tvDishName)
        tvIngredients = findViewById(R.id.tvIngredient)
        tvSteps = findViewById(R.id.tvSteps)
        btnDelete = findViewById(R.id.btnDelete)
        recipeID= intent.extras?.getString("recipeID")!!
        mRecipePresenterImpl = RecipePresenterImpl(this)

    }

    override fun onStart() {
        super.onStart()

        mRecipePresenterImpl?.onDisplaySingleRecipeDetails(recipeID)
        ivBack.setOnClickListener{
            finish()
            val intent = Intent(this, MainMenuActivity::class.java)
            startActivity(intent)
        }

        btnDelete.setOnClickListener{
            mRecipePresenterImpl?.onDeleteRecipe(recipeID)
        }


    }

    fun showError(msg: String)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun onNext(msg:String, recipe : RecipeInfo)
    {
//        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        tvRecipeName.text=recipe.getRecipeType()
        tvSteps.text = recipe.getSteps()
        tvIngredients.text= recipe.getIngredient()
        tvDishName.text=recipe.getDish()

        Glide
            .with(this)
            .load(Uri.parse(recipe.getImage()))
            .centerCrop()
            .thumbnail(0.1f)
            .into(ivDishImage)


    }

    fun showDeleteError(msg: String)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun onDeleteNext(msg:String, status:Boolean)
    {
        if(!status)
        {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }
        else
        {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
            finish()
            startActivity(
                Intent(this,
                    MainMenuActivity::class.java)
            )
        }
    }

    override fun onBackPressed() {
        finish()
        val intent = Intent(this, MainMenuActivity::class.java)
        startActivity(intent)
    }
}
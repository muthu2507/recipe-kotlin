package com.example.recipeapp.application.Recipe.view

import android.Manifest
import android.R.attr.path
import android.app.Activity
import android.app.ProgressDialog
import android.app.usage.NetworkStats
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.example.recipeapp.R
import com.example.recipeapp.application.Recipe.presenter.RecipePresenterImpl
import java.io.File
import java.security.Security


class add_recipe : AppCompatActivity(), View.OnClickListener {

    private lateinit var spinner: Spinner
    private lateinit var imageView: ImageView
    private lateinit var pickImage: Button
    private val REQUEST_PICK_PHOTO = 2

    private var mediaPath: String? = null

    private lateinit var pDialog: ProgressDialog
    private var postPath: String? = null

    private lateinit var recipe_name: EditText
    private lateinit var ingredients: EditText
    private lateinit var steps: EditText

    private var mRecipePresenterImpl: RecipePresenterImpl? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_recipe)

        val recipes = arrayOf(
            "Choose a recipe type",
            "Dessert",
            "Main Courses",
            "Salads",
            "Snacks"
        )

        spinner = findViewById(R.id.spinner);
        spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, recipes)

        recipe_name=findViewById(R.id.recipe_name)
        ingredients=findViewById(R.id.ingredients)
        steps=findViewById(R.id.steps)
        mRecipePresenterImpl = RecipePresenterImpl(this)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                //
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                //
            }
        }

        imageView = findViewById(R.id.preview) as ImageView
        pickImage = findViewById(R.id.pickImage) as Button
        pickImage.setOnClickListener(this)
        initDialog()

    }

    override fun onClick(v: View) {

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else {
            when (v.id) {
                R.id.pickImage -> MaterialDialog.Builder(this)
                    .title(R.string.uploadImages)
                    .items(R.array.uploadImages)
                    .itemsIds(R.array.itemIds)
                    .itemsCallback { dialog, view, which, text ->
                        when (which) {
                            0 -> {
                                val galleryIntent = Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                )
                                startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO)
                            }
                        }
                    }
                    .show()
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_PICK_PHOTO) {
                if (data != null) {
                    // Get the Image from data
                    val selectedImage = data.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor =
                        contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()

                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    mediaPath = cursor.getString(columnIndex)
                    // Set the Image in ImageView for Previewing the Media
                    imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
                    cursor.close()


                    postPath = mediaPath
                }


            }

        } else if (resultCode != Activity.RESULT_CANCELED) {
            Toast.makeText(this, "Sorry, there was an error!", Toast.LENGTH_LONG).show()
        }
    }

    protected fun initDialog() {

        pDialog = ProgressDialog(this)
        pDialog.setMessage(getString(R.string.msg_loading))
        pDialog.setCancelable(true)
    }





    fun submit(view: View) {

        if(recipe_name.text != null && ingredients.text !=null && steps.text!= null && mediaPath!=null && spinner.selectedItemPosition!=0){

            val uri = Uri.parse(mediaPath)
            mRecipePresenterImpl?.onCreateRecipe(recipe_name.text.toString(),ingredients.text.toString(),steps.text.toString(),spinner.selectedItem.toString(),uri)

        }
        else{
            Toast.makeText(this, "Please insert value for all the fields", Toast.LENGTH_LONG).show()
        }

    }

    fun showDeleteError(msg: String)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun onDeleteNext(msg:String, status:Boolean)
    {
        if(!status)
        {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }
        else
        {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
            finish()
            startActivity(
                Intent(this,
                    MainMenuActivity::class.java)
            )
        }
    }

    override fun onBackPressed() {
        finish()
        val intent = Intent(this, MainMenuActivity::class.java)
        startActivity(intent)
    }

}

package com.example.recipeapp.application.Utils



import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.recipeapp.R

open class SpinnerStyle(
    context: Context?,
    textViewResourceId: Int,
    private val objects: Array<String>
) :
    ArrayAdapter<String?>(context!!, textViewResourceId, objects) {
    override fun getDropDownView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        return getCustomView(position, convertView, parent)
    }

    private fun getCustomView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        val row: View =
            LayoutInflater.from(parent.context).inflate(R.layout.spinner_style, parent, false)
        val label = row.findViewById<View>(R.id.tvSpinnerValue) as TextView
        label.text = objects[position]
        return row
    }

}

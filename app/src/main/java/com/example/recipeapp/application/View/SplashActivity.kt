package com.example.recipeapp.application.View

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife
import com.example.recipeapp.R
import com.example.recipeapp.application.Recipe.view.MainMenuActivity
import com.example.recipeapp.application.authentication.view.LoginActivity


class SplashActivity : AppCompatActivity() {

    @BindView(R.id.llLogo)
    var llLogo: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        Log.d("TAG", "entering 1")

        llLogo?.startAnimation(AnimationUtils.loadAnimation(this,
            R.anim.splash_in
        ))
        Handler().postDelayed({
            llLogo?.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.splash_out
            ))
            Handler().postDelayed({
                llLogo?.visibility = View.GONE

                val sharedPreference =  getSharedPreferences("SHARED", Context.MODE_PRIVATE)
                var loggedin: Boolean= sharedPreference.getBoolean("loggedin",false)

                    startActivity(
                        Intent(
                            this,
                            LoginActivity::class.java
                        )
                    )



                finish()
            }, 500)
        }, 1500)



    }
}